# README

Test API with Pow and Sabisu.

### Install Pow

```
$ curl get.pow.cx | sh
```

Craete symlink to set up the Rack app.

```
$ cd ~/.pow
$ ln -s ~/work/miniwuaki
```

### Setup Sabisu

First run simple_form and sabisu_rails 
```
$ rails generate simple_form:install
$ rails generate sabisu_rails:install
```

Edit the file: config/initializers/sabisu_rails.rb

``` ruby
# Use this module to configure the sabisu available options

SabisuRails.setup do |config|

  # Base uri for posting the 
  config.base_api_uri = 'api.miniwuaki.dev'

  # Ignored attributes for building the forms
  # config.ignored_attributes = %w{ created_at updated_at id }
  
  # HTTP methods
  # config.http_methods = %w{ GET POST PUT DELETE PATCH }
  
  # Headers to include on each request
  #
  # You can configure the api headers fairly easy by just adding the correct headers
  config.api_headers = { "Accept" => "application/vnd.miniwuaki.v1" }
  #
  # config.api_headers = {}

  # Layout configuration
  # config.layout = "sabisu"

  # Resources on the api
  config.resources = [:users, :purchases, :sources, :movies, :seasons]
  
  # Default resource
  config.default_resource = :users

  # Application name
  # mattr_accessor :app_name
  # @@app_name = Rails.application.class.parent_name

  # Authentication
  # mattr_accessor :authentication_username
  # @@authentication_username = "admin"

  # mattr_accessor :authentication_password
  # @@authentication_password = "sekret"

end

```

On running server go to url: http://localhost:3000/sabisu_rails/explorer
