class Api::V1::EpisodesController < ApplicationController
  
  respond_to :json
  
  def index
    if params[:season_id]
      season = Season.find params[:season_id]
      respond_with season.episodes
    else
      respond_with Episode.all  
    end
  end
  
  def show
    if params[:season_id]
      season = Season.find params[:season_id]
      respond_with season.episodes.find params[:id]
    else
      respond_with Episode.find params[:id]
    end
  end
  
end
