class Api::V1::MoviesController < Api::V1::SourcesController
  
  respond_to :json
  
  def index
    if current_user
      items_purchased = current_user.purchases.active.map &:source_id
      respond_with Movie.where("id NOT IN (?)", items_purchased)
    else
      respond_with Movie.all
    end
  end

  def show
    respond_with Movie.find(params[:id])
  end
  
end
