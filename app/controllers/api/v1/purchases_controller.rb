class Api::V1::PurchasesController < ApplicationController
  before_action :authenticate_with_token!, only: [:show, :index, :create]
  respond_to :json
  
  def index
    if current_user && params[:user_id] && current_user.id.to_s == params[:user_id]
      respond_with Purchase.active.where(user_id: params[:user_id])
    else
      render json: { errors: "Unauthorized" }, status: :unauthorized
    end
  end
  
  def show
    if current_user
      purchase = current_user.purchases.find(params[:id])
      respond_with purchase
    else
      render json: { errors: "Unauthorized" }, status: :unauthorized
    end
  end
  
  def create
    items_purchased = current_user.purchases.active.map &:source_id
    if items_purchased.include?(purchase_params[:source_id].to_i)
      render json: { errors: "Already purchased" }, status: 409
    else
      purchase = current_user.purchases.build(purchase_params)
      purchase.end_date = DateTime.now + 2.days
      purchase.price = 2.99
      
      if purchase.save
        render json: purchase, status: 201, location: [:api, current_user, purchase]
      else
        render json: { errors: purchase.errors }, status: 422
      end
    end 
    
  end
  
  private

  def purchase_params
    params.require(:purchase).permit(:quality, :user_id, :source_id)
  end
  
end
