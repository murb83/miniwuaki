class Api::V1::SeasonsController < Api::V1::SourcesController
  
  respond_to :json
  
  def index
    if current_user
      items_purchased = current_user.purchases.active.map &:source_id
      respond_with Season.where("id NOT IN (?)", items_purchased)
    else
      respond_with Season.all.to_json(:include => [:episodes])
    end
  end

  def show
    respond_with Season.find(params[:id])
  end
  
end
