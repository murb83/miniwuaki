class Api::V1::SourcesController < ApplicationController
  
  respond_to :json
  
  def index
    if current_user
      items_purchased = current_user.purchases.active.map &:source_id
      respond_with Source.where("id NOT IN (?)", items_purchased)
    else
      respond_with Source.all  
    end
  end

  def show
    respond_with Source.find(params[:id])
  end
  
end
