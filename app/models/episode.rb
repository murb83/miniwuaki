class Episode < ActiveRecord::Base
  
  validates :name, :number, :season_id, presence: true
  
  belongs_to :season
  
  default_scope { order('number ASC') }
  
end
