class Purchase < ActiveRecord::Base
  validates :quality, :end_date, :user_id, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 2.99 },
                    presence: true
  belongs_to :user
  belongs_to :source
  
  delegate :movie, to: :source
  
  default_scope { order('end_date ASC') }
  
  scope :active, lambda {
    where("end_date > ?", DateTime.now)
  }
  
  scope :expired, lambda {
    where("end_date < ?", DateTime.now)
  }
  
  def expired?
    self.end_date < DateTime.now
  end
  
end
