class Source < ActiveRecord::Base
  
  validates :title, :plot, :type, presence: true
  has_many :purchases
  
  inheritance_column = :type
  
  default_scope { order('created_at DESC') } 
  scope :movies, -> { where(type: 'Movie') }
  scope :seasons, -> { where(type: 'Season') }
  
end
