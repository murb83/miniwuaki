class EpisodeSerializer < ActiveModel::Serializer
  cached
  attributes :id, :name, :number
  has_one :season
  
  def cache_key
    [object, scope]
  end
  
end
