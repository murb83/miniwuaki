class MovieSerializer < ActiveModel::Serializer
  cached
  #embed :ids
  attributes :id, :title, :plot, :type, :created_at
  #has_many :episodes
  
  def cache_key
    [object, scope]
  end
  
end
