class PurchaseSerializer < ActiveModel::Serializer
  cached
  attributes :id, :quality, :end_date, :expired?, :price, :created_at, :updated_at
  has_one :user
  has_one :source
  
  def cache_key
    [object, scope]
  end
  
end
