class SeasonSerializer < ActiveModel::Serializer
  cached
  # Bug on Serializer, "stack level too deep" when tries to embed objects (only works with ids). This will be fixed on 0.10 version.
  #embed :object
  attributes :id, :title, :plot, :type
  #has_many :episodes
  
  def cache_key
    [object, scope]
  end
  
end
