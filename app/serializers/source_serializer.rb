class SourceSerializer < ActiveModel::Serializer
  cached
  embed :ids
  attributes :id, :title, :plot, :type
  has_many :purchases
  
  def cache_key
    [object, scope]
  end
  
end
