require 'api_constraints'

Miniwuaki::Application.routes.draw do
  mount SabisuRails::Engine => "/sabisu_rails"
  devise_for :users
  # Api definition
  namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/'  do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      # We are going to list our resources here
      resources :users, :only => [:show, :create, :update, :destroy] do
        resources :purchases, :only => [:show, :index, :create, :update, :destroy] do
          resource :season
          resource :movie
          resources :movies, :only => [:show]
          resources :seasons, :only => [:show] do
            resources :episodes, :only => [:show, :index]
          end
        end
      end
      resources :sessions, :only => [:create, :destroy]
      resources :sources, :only => [:show, :index]
      resources :movies, :only => [:show, :index]
      resources :seasons, :only => [:show, :index] do
        resources :episodes, :only => [:show, :index]
      end
    end
  end
end
