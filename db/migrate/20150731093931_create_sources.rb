class CreateSources < ActiveRecord::Migration
  def change
    create_table :sources do |t|
      t.string :title, default: ""
      t.string :plot, default: ""
      t.string :type
      t.integer :season
      
      t.timestamps null: false
    end
    add_index :sources, :title
    add_index :sources, :type
  end
end
