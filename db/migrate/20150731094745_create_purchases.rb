class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.integer :user_id
      t.string :quality
      t.timestamp :end_date
      t.integer :source_id
      t.decimal :price, default: 0.0, precision: 5, scale: 2

      t.timestamps null: false
    end
    add_index :purchases, :user_id
    add_index :purchases, :end_date
  end
  
end
