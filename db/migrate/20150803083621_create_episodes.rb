class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.string :name
      t.integer :number
      t.integer :season_id

      t.timestamps null: false
    end
    add_index :episodes, :season_id
    add_index :episodes, :number
  end
end
