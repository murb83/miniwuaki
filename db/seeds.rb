# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

(1..10).each do |i|
  FactoryGirl.create :movie
end

(1..10).each do |i|
  s = FactoryGirl.create :season
  (1..24).each do |z|
    e = FactoryGirl.create :episode, season_id: s.id, number: z
  end
end

(1..5).each do |i| 
  u = FactoryGirl.create :user
  r = []
  n = 2+ Random.rand(10)
  while r.length < n 
    v = 1 + Random.rand(20)
    r << v unless r.include? v
  end
  r.each do |rand|
    end_date = Faker::Time.between(2.days.ago, 2.days.from_now)
    created_at = end_date - 2.days
    FactoryGirl.create :purchase, user_id: u.id, source_id: rand, end_date: end_date, created_at: created_at, updated_at: created_at
  end
  
end