require 'spec_helper'

RSpec.describe Api::V1::MoviesController, type: :controller do
  describe "GET #index" do
    
    before(:each) do
      4.times { 
        FactoryGirl.create :movie
      }
      get :index
    end
    
    it "returns 4 records from the database" do
      movies_response = json_response
      expect(movies_response[:movies].size).to eq(4)
    end
        
    it "retuns 4 records ordered by end_date DESC" do
      movies_response = json_response
      expect(movies_response[:movies].first[:created_at]).to be > movies_response[:movies].second[:created_at]
      expect(movies_response[:movies].second[:created_at]).to be > movies_response[:movies].third[:created_at]
      expect(movies_response[:movies].third[:created_at]).to be > movies_response[:movies].last[:created_at]
    end

    it { should respond_with 200 }
    
  end
end
