require 'spec_helper'

describe Api::V1::PurchasesController do

  describe "GET #show" do
    
    context "when user looks their purchases" do
    
      before(:each) do
        current_user = FactoryGirl.create :user
        movie = FactoryGirl.create :movie
        api_authorization_header current_user.auth_token
        @purchase = FactoryGirl.create :purchase, user_id: current_user.id, source_id: movie.id
        get :show, user_id: current_user.id, id: @purchase.id
      end

      it "returns the information about a reporter on a hash" do
        purchase_response = json_response[:purchase]
        expect(purchase_response[:end_date]).to eql @purchase.end_date.as_json
      end

      it { should respond_with 200 }
      
    end
    
  end
  
  describe "GET #index" do
    
    context "when user looks their purchases" do
      before(:each) do
        current_user = FactoryGirl.create :user
        4.times { 
          movie = FactoryGirl.create :movie
          FactoryGirl.create :purchase, user_id: current_user.id, source_id: movie.id
        }
        4.times {
          movie = FactoryGirl.create :movie
          FactoryGirl.create :purchase_expired, user_id: current_user.id, source_id: movie.id
        }
        api_authorization_header current_user.auth_token
        get :index, user_id: current_user
      end
      
      it "returns 4 records from the database" do
        purchases_response = json_response
        expect(purchases_response[:purchases].size).to eq(4)
      end
          
      it "retuns 4 records ordered by end_date ASC" do
        purchases_response = json_response
        expect(purchases_response[:purchases].first[:end_date]).to be < purchases_response[:purchases].second[:end_date]
        expect(purchases_response[:purchases].second[:end_date]).to be < purchases_response[:purchases].third[:end_date]
        expect(purchases_response[:purchases].third[:end_date]).to be < purchases_response[:purchases].last[:end_date]
      end

      it { should respond_with 200 }
    end
    
    context "when user tries to look on other user purchases" do 
      before(:each) do
        current_user = FactoryGirl.create :user
        other_user = FactoryGirl.create :user
        4.times { 
          movie = FactoryGirl.create :movie
          FactoryGirl.create :purchase, user_id: other_user.id, source_id: movie.id
        }
        api_authorization_header current_user.auth_token
        get :index, user_id: other_user
      end
      
      it "returns a json with an error" do
        expect(json_response[:errors]).to eql "Unauthorized"
      end

      it { should respond_with 401 }
    end
  end

  describe "POST #create" do
    
    context "when user purchase a movie" do 
    
      before(:each) do
        current_user = FactoryGirl.create :user
        api_authorization_header current_user.auth_token
        source = FactoryGirl.create :movie
        purchase_params = { quality: "HD", user_id: current_user.id, source_id: source.id}    
        post :create, user_id: current_user.id, purchase: purchase_params
      end

      it "returns the just user purchase record" do
        purchase_response = json_response[:purchase]
        expect(purchase_response[:id]).to be_present
      end
      
      it "purchase will end in two days" do
        purchase_response = json_response[:purchase]
        future = DateTime.now + 3.days
        expect(purchase_response[:end_date]).to be < future.as_json
      end
      
      it "purchase will have default price" do
        purchase_response = json_response[:purchase]
        expect(purchase_response[:price]).to eq(2.99.to_s)
      end

      it { should respond_with 201 }
      
    end
    
    context "when user purchase a movie already have" do 
    
      before(:each) do
        current_user = FactoryGirl.create :user
        api_authorization_header current_user.auth_token
        source = FactoryGirl.create :movie
        FactoryGirl.create :purchase, user_id: current_user.id, source_id: source.id
        purchase_params = { quality: "HD", user_id: current_user.id, source_id: source.id}    
        post :create, user_id: current_user.id, purchase: purchase_params
      end

      it "returns a json with an error" do
        expect(json_response[:errors]).to eql "Already purchased"
      end
      
      it { should respond_with 409 }
      
    end
    
    context "when user purchase a season" do 
    
      before(:each) do
        current_user = FactoryGirl.create :user
        api_authorization_header current_user.auth_token
        source = FactoryGirl.create :season
        purchase_params = { quality: "HD", user_id: current_user.id, source_id: source.id}    
        post :create, user_id: current_user.id, purchase: purchase_params
      end

      it "returns the just user purchase record" do
        purchase_response = json_response[:purchase]
        expect(purchase_response[:id]).to be_present
      end
      
      it "purchase will end in two days" do
        purchase_response = json_response[:purchase]
        future = DateTime.now + 3.days
        expect(purchase_response[:end_date]).to be < future.as_json
      end
      
      it "purchase will have default price" do
        purchase_response = json_response[:purchase]
        expect(purchase_response[:price]).to eq(2.99.to_s)
      end

      it { should respond_with 201 }
      
    end
    
    context "when user purchase a season already have" do 
    
      before(:each) do
        current_user = FactoryGirl.create :user
        api_authorization_header current_user.auth_token
        source = FactoryGirl.create :season
        FactoryGirl.create :purchase, user_id: current_user.id, source_id: source.id
        purchase_params = { quality: "HD", user_id: current_user.id, source_id: source.id}    
        post :create, user_id: current_user.id, purchase: purchase_params
      end

      it "returns a json with an error" do
        expect(json_response[:errors]).to eql "Already purchased"
      end
      
      it { should respond_with 409 }
      
    end
    
    context "when tries to purchase when if not logged in" do
      before(:each) do
        user = FactoryGirl.create :user
        source = FactoryGirl.create :season
        purchase_params = { quality: "HD", user_id: user.id, source_id: source.id}    
        post :create, user_id: user.id, purchase: purchase_params
      end

      it { should respond_with 401 }
    end 
  
    
  end
  
end
