require 'spec_helper'

RSpec.describe Api::V1::SeasonsController, type: :controller do
  describe "GET #index" do
    
    before(:each) do
      4.times { 
        season = FactoryGirl.create :season
        FactoryGirl.create :episode, season_id: season.id, number: 4
        FactoryGirl.create :episode, season_id: season.id, number: 2
        FactoryGirl.create :episode, season_id: season.id, number: 1
        FactoryGirl.create :episode, season_id: season.id, number: 3
      }
      get :index
    end
    
    it "returns 4 records from the database" do
      seasons_response = json_response
      expect(seasons_response.size).to eq(4)
    end
        
    it "retuns 4 records ordered by end_date DESC" do
      seasons_response = json_response
      expect(seasons_response.first[:created_at]).to be > seasons_response.second[:created_at]
      expect(seasons_response.second[:created_at]).to be > seasons_response.third[:created_at]
      expect(seasons_response.third[:created_at]).to be > seasons_response.last[:created_at]
    end
    
    it "season will have episodes" do
      seasons_response = json_response
      expect(seasons_response.first[:episodes]).not_to be_empty
    end
    
    it "season will have episodes ordered by episode number ASC" do
      seasons_response = json_response
      expect(seasons_response.first[:episodes].first[:number]).to eq(1)
      expect(seasons_response.first[:episodes].second[:number]).to eq(2)
      expect(seasons_response.first[:episodes].third[:number]).to eq(3)
      expect(seasons_response.first[:episodes].last[:number]).to eq(4)
    end

    it { should respond_with 200 }
    
  end
end
