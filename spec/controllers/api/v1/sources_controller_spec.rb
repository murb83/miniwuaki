require 'spec_helper'

describe Api::V1::SourcesController do
  
  describe "GET #show" do
    before(:each) do
      user = FactoryGirl.create :user
      @source = FactoryGirl.create :movie
      api_authorization_header user.auth_token
      get :show, id: @source.id
    end

    it "returns the information about a reporter on a hash" do
      source_response = json_response
      expect(source_response[:movie][:title]).to eql @source.title
      expect(source_response[:movie][:plot]).to eql @source.plot
    end

    it { should respond_with 200 }
  end
  
  describe "GET #index" do
    
    before(:each) do
      2.times { 
        FactoryGirl.create :movie
        FactoryGirl.create :season
       }
      get :index
    end
    
    it "returns four records from the database" do
      sources_response = json_response
      expect(sources_response[:sources].size).to eq(4)
    end

    it { should respond_with 200 }
  end
  
end
