FactoryGirl.define do
  
  factory :episode do
    name { FFaker::Movie.title }
    number { 1 }
    season_id { FactoryGirl.create(:season).id }
    created_at { Faker::Time.between(DateTime.now, 2.days.from_now) }
  end

end
