FactoryGirl.define do
  
  factory :movie do
    title { FFaker::Movie.title }
    plot { FFaker::Internet.http_url }
    created_at { Faker::Time.between(DateTime.now, 2.days.from_now) }
  end

end
