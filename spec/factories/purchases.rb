FactoryGirl.define do
  
  qly = ["SD", "HD"]
  
  factory :purchase do
    quality { qly[Random.rand(2)] }
    end_date { Faker::Time.forward(2) + 5.minutes }
    price { 2.99 }
    user
  end
  
  factory :purchase_expired, class: Purchase do
    quality { qly[Random.rand(2)] }
    end_date { Faker::Time.backward(30) }
    price { 2.99 }
    user
  end
end