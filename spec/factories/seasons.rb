FactoryGirl.define do
  
  factory :season do
    title { FFaker::Movie.title }
    plot { FFaker::Internet.http_url }
    created_at { Faker::Time.between(DateTime.now, 2.days.from_now) }
    season 1
  end

end
