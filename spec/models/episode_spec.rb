require 'spec_helper'

RSpec.describe Episode, type: :model do
  
  let(:episode) { FactoryGirl.build :episode }
  subject { episode }

  it { should respond_to(:name) }
  it { should respond_to(:number) }
  
  it { should validate_presence_of :name }
  it { should validate_presence_of :number }
  
  it { should belong_to :season }
  
  describe "when number is not present" do
    before { episode.number = nil }
    it { should_not be_valid }
  end
  
end
