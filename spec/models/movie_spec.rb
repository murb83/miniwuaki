require 'spec_helper'

RSpec.describe Movie, type: :model do
  
  let(:movie) { FactoryGirl.build :movie }
  subject { movie }

  it { should respond_to(:title) }
  it { should respond_to(:plot) }
  it { should respond_to(:type) }
  
  it { should validate_presence_of :title }
  it { should validate_presence_of :plot }
  it { should validate_presence_of :type }
  
  it { should have_many :purchases }
  
  describe "when type is not present" do
    before { movie.type = nil }
    it { should_not be_valid }
  end
  
end
