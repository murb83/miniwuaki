require 'spec_helper'

RSpec.describe Purchase, type: :model do
  let(:purchase) { FactoryGirl.build :purchase }
  subject { purchase }

  it { should respond_to(:user_id) }
  it { should respond_to(:quality) }
  it { should respond_to(:end_date) }
  it { should respond_to(:source_id) }
  it { should validate_numericality_of(:price).is_greater_than_or_equal_to(2.99) }
  
  it { should belong_to :user }
  
  it { should belong_to(:source) }
  
  describe "when end date is not present" do
    before { purchase.end_date = " " }
    it { should_not be_valid }
  end
  
end
