require 'spec_helper'

RSpec.describe Source, type: :model do
  
  describe "#movie inheritance" do

    before do
      movie = FactoryGirl.create :movie
    end

    it "its a movie" do
      s = Source.first
      expect(s.class).to eql Movie
    end
  end
  
  describe "#season inheritance" do

    before do
      movie = FactoryGirl.create :season
    end

    it "its a season" do
      s = Source.first
      expect(s.class).to eql Season
    end
  end
  
end
